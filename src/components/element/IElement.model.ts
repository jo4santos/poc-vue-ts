export interface IElement {
    id: number,
    title:string,
    description:string
}