import Vue from 'vue';
import Component from 'vue-class-component';
import {Prop} from 'vue-property-decorator';
import template from './element.component.html';
import style from './element.component.scss';

@Component({
    template: template
})
export default class FzElement extends Vue {

    @Prop({default: 'Default title'})
    title: string;

    @Prop({default: 'Default description'})
    description: string;

    collapsed: boolean = true;
    loading: boolean = false;
    loadedValue: number = 0;

    toggleCollapsed() {
        this.collapsed = !this.collapsed;
    }

    getTitle() {
        return `_${this.title}_`;
    }
    getDescription() {
        return this.description;
    }

    loadSomething() {
        this.loading = true;
        this.doLoadSomething()
            .then(sum => this.loadedValue = sum)
            .then(() => this.loading = false);
    }

    async doLoadSomething(): Promise<number> {
        const a = await this.resolveAfter2Seconds();
        const b = await this.resolveAfter2Seconds();
        const sum = a + b;
        return sum;
    }

    resolveAfter2Seconds(): Promise<number> {
        return new Promise(resolve => {
            setTimeout(() => {
                resolve(Math.random());
            }, 2000);
        });
    }
}