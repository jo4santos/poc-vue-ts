import Vue from 'vue';
import FzHomeComponent from './components/home/home.component';
import FzElementComponent from './components/element/element.component';

Vue.component('fz-home', FzHomeComponent);
Vue.component('fz-element', FzElementComponent);

new Vue({
  el: '#fz-app'
})
