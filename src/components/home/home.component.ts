import Vue from 'vue';
import Component from 'vue-class-component';

import FzElementComponent from "../element/element.component";
import {IElement} from "../element/IElement.model";

import template from './home.component.html';

@Component({
    components: { FzElementComponent },
    template: template
})

export default class FzHome extends Vue {
    inputNumberElements: number = 1;
    inputTitleElements: string = "!";
    elements: IElement[] = [];

    addElement(numberElements: number) {
        let existingElements = this.elements.length;
        for (let i: number = 0; i < numberElements; i++) {
            this.elements.push({
                id: i,
                title: `Element ${existingElements + i}`,
                description: `Description of ${existingElements + i}`
            });
        }
    }
    addElementsInput() {
        this.addElement(this.inputNumberElements);
    }
    changeElementTitles() {
        for (let element of this.elements)
            element.title += " " + this.inputTitleElements;
    }
}